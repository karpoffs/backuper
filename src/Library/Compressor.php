<?php

namespace Library;

use Interfaces\CompressorInterface;

/**
 * Class Compressor
 *
 * @package Library
 */
class Compressor  implements CompressorInterface
{
	const KEY_EXCLUDES = '--exclude=';
	const KEY_IGNORE_FAILED_READ = '--ignore-failed-read';
	const KEY_ABSOLUTE_NAMES = '--absolute-names';

	protected $template = 'tar %s %s %s %s';

	/**
	 * @var string
	 */
	protected $fileName;
	
	/**
	 * @var string
	 */
	protected $extFile;

	/**
	 * @var null|string
	 */
    protected $type = null;

    /**
     * @var bool
     */
    protected $ignoreFailedRead = false;
    
	/**
	 * @var  string
	 */
	private $path;

	/**
	 * @var  string
	 */
	private $savePath;

	/**
	 * @var array
	 */
	private $includes = [];

	/**
	 * @var array
	 */
	private $excludes = [];

	/**
	 * Compressor constructor.
	 *
	 * @param string $type
	 */
    public function __construct(string $fileName, string $type)
    {
        $this->type = strtolower($type);
        $this->fileName = $fileName;
    }

	public function setSavePath(string $path)
	{
		return $this->savePath = $path;
	}

	/**
	 * @return string
	 */
	public function getExtFile()
	{
		return $this->extFile;
	}

	/**
	 * @return string
	 */
	public function getFullFileName()
	{
		if ($this->type === CompressorInterface::COMPRESSOR_GZIP){

			$this->extFile = implode(
				'.',
				[
					CompressorInterface::COMPRESSOR_TAR,
					CompressorInterface::COMPRESSOR_GZIP
				]
			);
			return implode(
				'.',
				[
					$this->fileName,
					$this->extFile
				]
			);
		} elseif ($this->type === CompressorInterface::COMPRESSOR_BZIP2){
			$this->extFile = implode(
				'.',
				[
					CompressorInterface::COMPRESSOR_TAR,
					CompressorInterface::COMPRESSOR_BZIP2
				]
			);
			return implode(
				'.',
				[
					$this->fileName,
					$this->extFile
				]
			);
		} elseif ($this->type === CompressorInterface::COMPRESSOR_LZMA){
			$this->extFile = implode(
				'.',
				[
					CompressorInterface::COMPRESSOR_TAR,
					CompressorInterface::COMPRESSOR_LZMA
				]
			);
			return implode(
				'.',
				[
					$this->fileName,
					$this->extFile
				]
			);
		}
		return implode('.',[$this->fileName, CompressorInterface::COMPRESSOR_TAR]);
	}

	public function getFullPathForSaveFileName(){
		return implode(
			DIRECTORY_SEPARATOR,
			[
				rtrim($this->savePath, DIRECTORY_SEPARATOR),
				$this->getFullFileName()
			]
		);
	}

	/**
	 * @param bool $bool
	 */
	public function setIgnoreFailedRead(bool $bool)
	{
		$this->ignoreFailedRead = $bool;
	}

	/**
	 * @param string $path
	 */
	public function setPath(string $path)
	{
		$this->path = $path;
	}

	/**
	 * @param string $path
	 */
	public function addInclude(string $path)
	{
		$this->includes[] = $path;
	}

	/**
	 * @param string $path
	 */
	public function addExclude(string $path)
	{
		$this->excludes[] = $path;
	}

	/**
	 * @return string
	 */
	public function compile()
	{
		// Получаем пути для включения архивации
		$includes = $this->generateIncludes();

		// Получаем пути исключённые из процесса архивации
		$excludes = $this->generateExcludes();

		$compress = '-cvf';
		$archiveFullPath = '"'.$this->getFullPathForSaveFileName().'"';
		$cmd = sprintf($this->template, $compress, $archiveFullPath, $excludes, $includes);

		if ($this->type === CompressorInterface::COMPRESSOR_GZIP){
			$compress = '-cvzf';
			$cmd = sprintf($this->template, $compress, $archiveFullPath, $excludes, $includes);
		} elseif ($this->type === CompressorInterface::COMPRESSOR_BZIP2){
			$compress = '-cvjf';
			$cmd = sprintf($this->template, $compress, $archiveFullPath, $excludes, $includes);
		} elseif ($this->type === CompressorInterface::COMPRESSOR_LZMA){
			$compress = '-cvf';
			$cmd = sprintf($this->template.' %s', $compress, $archiveFullPath, '--lzma', $excludes, $includes);
		}

		$cmd = trim($cmd);

		// абсолютные имена
		$cmd.= ' '.self::KEY_ABSOLUTE_NAMES;

		if($this->ignoreFailedRead){
			$cmd.= ' '.self::KEY_IGNORE_FAILED_READ;
		}
		return $cmd;
	}

	/**
	 * Общяя функция для генерации путей
	 * @param array $items
	 * @return array
	 */
	private final function generatePaths(array $items){
		return array_map(
			function ($item){
				// create path
				return implode(
					DIRECTORY_SEPARATOR,
					[
						rtrim($this->path, DIRECTORY_SEPARATOR),
						trim($item, DIRECTORY_SEPARATOR)
					]
				);
			},
			$items
		);
	}

	/**
	 *  Генерирует пути для включения в процесс архивации
	 * @return string
	 */
    private function generateIncludes()
    {
	    if(count($this->includes)){
		    return implode(
			    ' ',
			    // generate paths
			    $this->generatePaths($this->includes)
		    );
	    }
		return $this->path;
    }

	/**
	 * Генерирует пути для искючения из процесса архивации
	 * @return string
	 */
    private function generateExcludes()
    {
	    if(count($this->excludes)){
		    return implode(' ',
			    // appends key excludes
			    array_map(
				    function ($path){
					    return  self::KEY_EXCLUDES.$path;
				    },
				    // generate paths
				    $this->generatePaths($this->excludes)
			    )
		    );
	    }
		return '';
    }

	/**
	 * @return array
	 */
	public static function getSupported(): array
	{
		return [
			CompressorInterface::COMPRESSOR_TAR,
			CompressorInterface::COMPRESSOR_GZIP,
			CompressorInterface::COMPRESSOR_BZIP2,
			CompressorInterface::COMPRESSOR_LZMA
		];
	}
}