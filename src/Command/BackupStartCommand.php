<?php

namespace Command;

use Interfaces\CompressorInterface;
use Library\Compressor;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Helper\TableCell;
use Symfony\Component\Console\Helper\TableSeparator;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Finder\Finder;
use Utils\ExchangeData;
use Utils\HelperFunctions;

/**
 * Class BackupStartCommand
 * @package Command
 */
class BackupStartCommand extends Command
{
	// otherwise options
	const OPTION_SHOW_ONLY_TARGETS = 'show-only-targets';
	const OPTION_MINIMUM_DISK_FREE_SPACE = 'minimum-disk-free-space';

	/**
	 * @var array
	 */
	private $config = [];

	/**
	 * @var string|null
	 */
	private $backupPath = null;

	/**
	 * @var string
	 */
	private $defaultCompressor;

	/**
	 * @var LoggerInterface
	 */
	private $logger;

	/**
	 * @var Finder
	 */
	private $finder;

	/**
	 * Заголовки таблиц
	 * @var array
	 */
	private $tableHeaders = [
		'targets' => ['Targets', 'Compressor', 'Path', 'Includes', 'Excludes'],
		'stateTargets' => ['Targets', 'Archive', 'Size', 'State'],
		'cleanBackups' => ['Path', 'Size', 'State'],
	];

	/**
	 * @var array
	 */
	protected $defaultItem = [
        'compressor' => '',
		'ignoreFailedRead' => false,
        'path' => '',
		'include' => [],
        'exclude' => []
	];


	/**
	 * BackupStartCommand constructor.
	 * @param array $config
	 * @param LoggerInterface $logger
	 * @param string|null $name
	 */
	public function __construct(array $config, LoggerInterface $logger, string $name = null)
	{
		$this->finder = new Finder();
		$this->logger = $logger;
		$this->config = $config['backuper'];
		$this->backupPath = BACKUP_PATH;

		if(isset($config['backuper']['ignoreFailedRead'])){
			$this->config['ignoreFailedRead'] = (bool) $config['backuper']['ignoreFailedRead'];
		} else {
			$this->config['ignoreFailedRead'] = false;
		}

		if(isset($this->config['default_compressor'])){
			if($this->config['default_compressor'] === null){
				$this->defaultCompressor = CompressorInterface::COMPRESSOR_TAR;
			} else {
				if(!in_array($this->config['default_compressor'], Compressor::getSupported())){
					$this->logger->info(
						'Compressor is not supported',
						['default_compressor' => $this->config['default_compressor'] ]
					);
					$this->defaultCompressor = CompressorInterface::COMPRESSOR_TAR;
				} else {
					$this->defaultCompressor = $this->config['default_compressor'];
				}
			}
		} else {
			$this->defaultCompressor = CompressorInterface::COMPRESSOR_TAR;
		}
		parent::__construct($name);
	}

	/**
	 * Настройка команды
	 */
	protected function configure()
	{
		$this
			->setName('backup:start')
			->setDescription('Выполняет резервное копирование файлов')
			->addOption(self::OPTION_SHOW_ONLY_TARGETS, null, InputOption::VALUE_OPTIONAL, 'Выводить таблицу и выходит', false)
			->addOption(self::OPTION_MINIMUM_DISK_FREE_SPACE, null, InputOption::VALUE_OPTIONAL, 'Минимально допустимое дисковое пространство', 5368709120)
		;
	}

	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$io = new SymfonyStyle($input, $output);
		try{

			$io->newLine();
			$io->title(
				'Дата и время запуска выполенения резервного копирования '.
				(new \DateTime('now'))->format('Y-m-d H:i:s')
			);

			// Выводим таблицу целей
			$this->showTableTargets($output);

			$io->newLine();

			// Если флаг только вывести таблицу
			if($input->getOption(self::OPTION_SHOW_ONLY_TARGETS)){
				return true;
			}

			$minimumFreeSpace = $input->getOption(self::OPTION_MINIMUM_DISK_FREE_SPACE);
			// проверка дискового пространства
			if(!$this->checkDiskFreeSpace($this->backupPath, $minimumFreeSpace)){
				throw new \Exception('Недостаточно дискового пространства для выполнения резервоно копирования, требуется не менее ' . HelperFunctions::human_filesize($minimumFreeSpace), 100);
			}

			// Накапливаем состояние резервного копирвания
			$statusTargets = [];

			// Накапливаем состояние очистки устаревших архивов
			$deleteOldBackups = [];

			// Если есть что что бекапить
			if(isset($this->config['folders']) && is_array($this->config['folders'])){

				// Загружаем предыдущие данные
				$exchange = new ExchangeData(CACHE_PATH);

				$folders = $this->config['folders'];
				foreach ($folders as $name=>$item) {

					$nowDate = new \DateTime('now');
					$humanDate = $nowDate->format($this->config['format_date']);

					// Получаем текущие настройки
					if(is_array($item)){
						$settings = $this->getItemSettings($item);
					} else {
						$settings = $this->getItemSettings([ 'path' => $item]);
					}

					// Если есть команды до выполнения основного задания
					if(isset($settings['commands'])){
						if(isset($settings['commands']['before'])){
							foreach ($settings['commands']['before'] as $command){
								$this->runExternalCommand($command, ['Command','Before']);
							}
						}
					}

					// Если установлен флаг очистки старых резервных копий
					if(isset($this->config['cleanBackups'])){
						$this->finder->in($this->backupPath)->files()
							->name($name.'*')->date($this->config['cleanBackups']);

						foreach ($this->finder as $file) {

							// массив файла
							$fileItem = [
								'target' => $file->getRealPath(),
								'sizeBytes' => $file->getSize()
							];

							if(unlink($file->getRealPath())){
								$deleteOldBackups[] = array_merge($fileItem, ['state' => 'success']);
								$this->logger->notice(
									'[Delete]: Older backup file '. $file->getRelativePathname() .
									' in directory '. $file->getPath()
								);
							} else {
								$deleteOldBackups[] = array_merge($fileItem, ['state' => 'error']);
								$this->logger->warning(
									'[Delete]: file not delete'. $file->getRelativePathname() .
									' in directory '. $file->getPath()
								);
							}
						}
					}

					// Проверка во время выполнения
					if(!$this->checkDiskFreeSpace($this->backupPath, $minimumFreeSpace)){
						throw new \Exception('Во время выполения резервного копирования внезапно кончилось доступное дисковое пространство, требуется не менее ' . HelperFunctions::human_filesize($minimumFreeSpace), 100);
					}
					
					// Создаём объект сборшика данных
					$archive = new Compressor(
						$name .'-'.$humanDate,
						$settings['compressor']
					);
					$archive->setSavePath($this->backupPath);
					$archive->setPath($settings['path']);
					$archive->setIgnoreFailedRead($settings['ignoreFailedRead']);

					if(isset($settings['include'])){
						foreach($settings['include'] as $include){
							if(is_string($include)){
								$archive->addInclude($include);
							}
						}
					}
					
					if(isset($settings['exclude'])){
						foreach($settings['exclude'] as $exclude){
							if(is_string($exclude)){
								$archive->addExclude($exclude);
							}
						}
					}

					// Статистика
					$stats = [
						'folders' => 0,
						'files' => 0
					];

					// Если есть команда
					if(strlen($cmd = $archive->compile())){

						// Запись в лог
						$this->logger->info('[Command]: '.$cmd);

						// Создаём процесс
						$process = new Process($cmd);

						// Если текущий
						if(substr($settings['path'], 0,1) === '/'){
							$process->setWorkingDirectory('/');
						}

						// Отключаем лимит выполнения команды
						$process->setTimeout(null);
						// Запускаем процесс
						$process->run(function ($type, $buffer) use(&$stats) {
							if (Process::ERR === $type) {
								$this->logger->warning($buffer);
							} else {
								$paths = explode(PHP_EOL, $buffer);
								foreach ($paths as $path){
									if(mb_strlen($path)){
										if(substr($path, -1) === DIRECTORY_SEPARATOR){
											$stats['folders']++;
										} else {
											$stats['files']++;
										}
										$this->logger->info('[Path]: '. $path);
									}
								}
							}
						});

						// Если что-то пошло не так то сообщаем
						if (!$process->isSuccessful()) {
							if(file_exists($archive->getFullPathForSaveFileName())){
								unlink($archive->getFullPathForSaveFileName());
								$this->logger->critical('[Archive][Removed]: '. $archive->getFullPathForSaveFileName());
								$this->logger->critical('[Archive][Reason]: '. $process->getErrorOutput());
							}
							$statusTargets[] = [
								'target' => $name,
								'archive' => $archive->getFullPathForSaveFileName(),
								'sizeBytes' => filesize($archive->getFullPathForSaveFileName()),
								'state' => 'error',
							];
							continue;
						}

						// Добовляем данные о новом файле
						$exchange->addCurrentFile(
							array_merge(
								[ 'mask' => $name ], // маска имени файла
								HelperFunctions::hash_file_multi(
									['md5', 'sha1', 'sha256'],
									$archive->getFullPathForSaveFileName()
								),
								[ 'ext' => $archive->getExtFile()],  // расширение файла
								[ 'unix_timestamp' => $nowDate->getTimestamp()], // Время в unixtimestamp
								[ 'human_date' => $humanDate ], // Время в формате \DateTime::ATOM
								[ 'stats' =>  $stats ]  // Статистика
							)
						);

						// Результат работы в лог
						$this->logger->emergency(
							sprintf(
								'Reserve copy files %d pcs. in %d directories.',
								$stats['files'],
								$stats['folders']
							)
						);

						$statusTargets[] = [
							'target' => $name,
							'archive' => $archive->getFullPathForSaveFileName(),
							'sizeBytes' => filesize($archive->getFullPathForSaveFileName()),
							'state' => 'success',
						];
					}

					// Если есть команды после резервного копирвования
					if(isset($settings['commands'])){
						if(isset($settings['commands']['after'])){
							foreach ($settings['commands']['after'] as $command){
								$this->runExternalCommand($command, ['Command','After']);
							}
						}
					}
				}

				$exchange->saveData();

				if(count($deleteOldBackups)){
					// Выводим таблицу удалённых бакапов
					$this->showTableCleanBackups($output, $deleteOldBackups);
					$io->newLine();
				}

				if(count($statusTargets)){
					// Выводим таблицу выполненых целей
					$this->showTableResultTargets($output, $statusTargets);
					$io->newLine();
				}

			} else {
				throw new \Exception('Отсутвуют цели для резервного копирования', 125);
			}
		} catch (\Exception $e){
			$this->logger->error($e->getMessage());
			$io->error($e->getMessage());
			return 255;
		}

		return 0;
	}

	/**
	 * Выполнение внешних команд
	 * 
	 * @param string $command
	 * @param array  $context
	 */
	private function runExternalCommand(string $command, array $context)
	{
		$context = implode('', array_map(function ($s){return '['.$s.']';}, $context));
		// Запись в лог
		$this->logger->info($context.': '.$command);
		$process = new Process($command);
		$process->setTimeout(null);
		$process->run(function ($type, $buffer) use($context) {
			if (Process::ERR === $type) {
				$this->logger->warning($context.': '. $buffer);
			} else {
				$strings = explode(PHP_EOL, $buffer);
				foreach ($strings as $string){
					if(mb_strlen($string)){
						$this->logger->info($context.': '. $string);
					}
				}
			}
		});

		if (!$process->isSuccessful()) {
			throw new ProcessFailedException($process);
		}
	}

	/**
	 * @param array $item
	 * @return array
	 */
	private function getItemSettings(array $item)
	{
		if(!isset($item['compressor'])){
			$item['compressor'] = $this->defaultCompressor;
		} else {
			$item['compressor'] = isset($item['compressor']) ?
				$this->getSupportedCompressor($item['compressor']) :
				$this->defaultCompressor;
		}

		if(!isset($item['ignoreFailedRead'])){
			$item['ignoreFailedRead'] = $this->config['ignoreFailedRead'];
		}

		return array_merge($this->defaultItem, $item);
	}

	/**
	 * Объщая функция рендеринга таблицы
	 * @param OutputInterface $output
	 * @param array           $headers
	 * @param array           $rows
	 */
	final protected function renderTable(OutputInterface $output, array $headers = [], array $rows = [])
	{
		$table = new Table($output);

		// Устанавливаем заголовки
		$table->setHeaders($headers);

		// Устанавливаем строки
		$table->setRows($rows);

		// Выводим таблицу
		$table->render();
	}

	/**
	 * Создаём строки для таблицы 
	 *
	 * @param array $targets
	 * @param null  $columnCount
	 * @return array
	 */
	private final function getTargetsRows(array $targets = [], $columnCount = null)
	{
		$rows = [];
		$countTargets = count($targets)-1;
		$counts = 0;
		foreach ($targets as $name=>$target) {
			if(is_array($target)){
				$settings = $this->getItemSettings($target);
			} else {
				$settings = $this->getItemSettings([ 'path' => $target]);
			}

			$rows[] = [
				$name,
				$settings['compressor'],
				$settings['path'],
				implode("\n", $settings['include']),
				implode("\n", $settings['exclude'])
			];
			if($countTargets !== $counts){
				$rows[] = new TableSeparator();
			}
			$counts++;
		}

		if($counts > 0 ) {
			if($columnCount !== null){
				$rows[] = new TableSeparator();
				$rows[] = [
					new TableCell(
						'Всего найдено целей - '.$counts.' шт.',
						['colspan' => $columnCount]
					)
				];
			}
		} else {
			if($columnCount !== null){
				$rows[] = [
					new TableCell(
						'Целей не найдено.',
						['colspan' => $columnCount]
					)
				];
			}
		}
		return $rows;
	}

	/**
	 * Вывод удалённых или попыток удаления устаревшых бекапов
	 * @param array $items
	 * @param null  $columnCount
	 * @return array
	 */
	private final function getCleanBackupsRows(array $items = [], $columnCount = null)
	{
		$rows = [];
		$countSuccess = 0;
		$countErrors = 0;
		$countTargets = count($items) - 1;
		$counts = 0;

		$successSizes = 0;
		$errorSizes = 0;
		foreach ($items as $name=>$item) {
			$rows[] = [
				$item['target'],
				HelperFunctions::human_filesize($item['sizeBytes']),
				$item['state'] === 'success' ? '✔' : '✘'
			];

			if($countTargets !== $counts){
				$rows[] = new TableSeparator();
			}

			if($item['state'] === 'success'){
				$successSizes += $item['sizeBytes'];
				$countSuccess++;
			} else {
				$errorSizes += $item['sizeBytes'];
				$countErrors++;
			}

			$counts++;
		}

		if($countSuccess > 0 ) {
			if($columnCount !== null){
				$rows[] = new TableSeparator();
				$rows[] = [
					new TableCell(
						'Всего удалено файлов - '.$countSuccess.' шт. освобождено дискового пространства - '.HelperFunctions::human_filesize($successSizes),
						['colspan' => $columnCount]
					)
				];
			}
		}

		if($countErrors > 0) {
			if($columnCount !== null){
				$rows[] = new TableSeparator();
				$rows[] = [
					new TableCell(
						'Всего опущено удаление файлов - '.$countErrors.' шт.'.
						' объёмом '.HelperFunctions::human_filesize($errorSizes),
						['colspan' => $columnCount]
					)
				];
			}
		}
		return $rows;
	}

	/**
	 * Вывод удалённых или попыток удаления устаревшых бекапов
	 * @param array $items
	 * @param null  $columnCount
	 * @return array
	 */
	private final function getStateTargetsRows(array $items = [], $columnCount = null)
	{
		$rows = [];
		$countSuccess = 0;
		$countErrors = 0;
		$countTargets = count($items) - 1;
		$counts = 0;

		$successSizes = 0;
		$errorSizes = 0;
		foreach ($items as $item) {
			$rows[] = [
				$item['target'],
				$item['archive'],
				HelperFunctions::human_filesize($item['sizeBytes']),
				$item['state'] === 'success' ? '✔' : '✘'
			];
			
			if($countTargets !== $counts){
				$rows[] = new TableSeparator();
			}

			if($item['state'] === 'success'){
				$successSizes += $item['sizeBytes'];
				$countSuccess++;
			} else {
				$errorSizes += $item['sizeBytes'];
				$countErrors++;
			}

			$counts++;
		}

		if($countSuccess > 0 ) {
			if($columnCount !== null){
				$rows[] = new TableSeparator();
				$rows[] = [
					new TableCell(
						'Создано резерных копий - '.$countSuccess.' шт. объёмом - '.HelperFunctions::human_filesize($successSizes),
						['colspan' => $columnCount]
					)
				];
			}
		}

		if($countErrors > 0) {
			if($columnCount !== null){
				$rows[] = new TableSeparator();
				$rows[] = [
					new TableCell(
						'Пропущено создание резерных копий - '.$countErrors.' шт.'.
						' объёмом '.HelperFunctions::human_filesize($errorSizes),
						['colspan' => $columnCount]
					)
				];
			}
		}
		return $rows;
	}

	/**
	 * @param string $compressor
	 * @param array  $context
	 * @return string
	 */
	protected function getSupportedCompressor(string $compressor, array $context = [])
	{
		if(in_array($compressor, Compressor::getSupported())){
			return $compressor;
		} else {
			$this->logger->warning(
				'Compressor not supported, choice default compressor ('.$this->defaultCompressor.')',
				$context ?? Compressor::getSupported()
			);
			return $this->defaultCompressor;
		}
	}

	/**
	 * @param OutputInterface $output
	 */
	private function showTableTargets(OutputInterface $output): void
	{
		$columnCount = count($this->tableHeaders['targets']);
		$this->renderTable(
			$output,
			[
				[
					new TableCell(
						'Цели резервного копирования',
						['colspan' => $columnCount]
					)
				],
				$this->tableHeaders['targets']
			],
			// Формируем строки
			$this->getTargetsRows(
				$this->config['folders'] ?? [],
				$columnCount
			)
		);
	}

	/**
	 * @param OutputInterface $output
	 * @param array           $deleteOldBackups
	 */
	protected function showTableCleanBackups(OutputInterface $output, array $deleteOldBackups = []): void
	{
		$columnCount = count($this->tableHeaders['cleanBackups']);
		$this->renderTable(
			$output,
			[
				[
					new TableCell(
						'Очистка устаревших резервных копирований',
						['colspan' => $columnCount]
					)
				],
				$this->tableHeaders['cleanBackups']
			],
			// Формируем строки
			$this->getCleanBackupsRows(
				$deleteOldBackups,
				$columnCount
			)
		);
	}

	/**
	 * @param OutputInterface $output
	 * @param array           $statusTargets
	 */
	private function showTableResultTargets(OutputInterface $output, array $statusTargets = []): void
	{
		$columnCount = count($this->tableHeaders['stateTargets']);
		$this->renderTable(
			$output,
			[
				[
					new TableCell(
						'Результат выполнения резервного копирования',
						['colspan' => $columnCount]
					)
				],
				$this->tableHeaders['stateTargets']
			],
			// Формируем строки
			$this->getStateTargetsRows(
				$statusTargets,
				$columnCount
			)
		);
	}

	/**
	 * @param $minimumFreeSpace
	 * @return bool
	 */
	protected function checkDiskFreeSpace(string $path, int $minimumFreeSpace): bool
	{
		$freeSpace = disk_free_space($path);
		if ($freeSpace !== false && $freeSpace < $minimumFreeSpace) {
			return false;
		}
		return true;
	}

}
